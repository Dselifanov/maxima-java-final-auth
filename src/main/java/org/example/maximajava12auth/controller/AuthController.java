package org.example.maximajava12auth.controller;


import org.example.maximajava12auth.model.JwtToken;
import org.example.maximajava12auth.service.JwtTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthController {

    @Autowired private JwtTokenService tokenService;

    @PostMapping("/auth")
    public JwtToken hello(Authentication auth){
        return new JwtToken(tokenService.generateToken(auth));
    }
}
