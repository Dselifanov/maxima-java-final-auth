package org.example.maximajava12auth.repository;

import org.example.maximajava12auth.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountRepository extends JpaRepository<UserAccount, String> {
}
